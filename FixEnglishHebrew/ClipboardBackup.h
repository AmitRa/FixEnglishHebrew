#pragma once

#include "LinkedList.h"
#include <Windows.h>

typedef struct ClipboardData
{
	ClipboardData() : m_nFormat(0), m_nLength(0), m_pData(nullptr)
	{
	}
	ClipboardData(const ClipboardData& data) : m_nFormat(data.m_nFormat), m_nLength(data.m_nLength), m_pData(new byte[data.m_nLength])
	{
		std::memcpy(m_szFormatName, data.m_szFormatName, 100);
		std::memcpy(m_pData, data.m_pData, data.m_nLength);
	}
	~ClipboardData()
	{
		delete[] m_pData;
	}

	UINT m_nFormat;
	wchar_t m_szFormatName[100];
	SIZE_T m_nLength;
	LPVOID m_pData;
} ClipboardData;

class CClipboardBackup
{
public:
	CClipboardBackup();
	~CClipboardBackup();

	bool Backup ();
	bool Restore();

private:
	void ClearBackupDatas();
	LinkedList<ClipboardData> m_lstData;
};
