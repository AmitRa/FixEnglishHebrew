#include "ClipboardBackup.h"
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <array>
#include <codecvt>

#define HOTKEY_ID 1337
#define NUM_OF_KEYS 33

void waitForCtrlF10(std::condition_variable& f10Cond);
void fakeF10Event();
BOOL WINAPI CtrlHandler(DWORD fdwCtrlType);
void copySelectedText();
std::wstring getCopiedVal();
std::wstring stringToWstring(const char* utf8Bytes);
void handleData();
std::wstring fixEnglishHebrew(const std::wstring& data);
std::wstring switchKeyboards(const std::wstring& data, const std::array<wchar_t, NUM_OF_KEYS>& source, const std::array<wchar_t, NUM_OF_KEYS>& target);
void flushFixedString(const std::wstring& fixedString);
void pasteResult();
std::wstring toLower(const std::wstring& data);
void selectAll();

bool globalTeminate = false;

int main()
{
	::ShowWindow(::GetConsoleWindow(), SW_HIDE);
	SetConsoleCtrlHandler(CtrlHandler, TRUE);
	std::condition_variable f10Cond;
	std::mutex f10Mutex;
	std::unique_lock<std::mutex> f10Lock(f10Mutex);
	std::thread eventCapture(waitForCtrlF10, std::ref(f10Cond));
	

	while (!globalTeminate)
	{
		f10Cond.wait(f10Lock);
		if (!globalTeminate)
		{
			handleData();
		}
	}

	eventCapture.join();
	return 0;
}

void handleData()
{
	CClipboardBackup Backupclipboard;
	std::wstring lastDataBeforeCopy = getCopiedVal();

	copySelectedText();
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	std::wstring copiedData = getCopiedVal();
	if (copiedData == lastDataBeforeCopy)
	{
		selectAll();
		copySelectedText();
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		copiedData = getCopiedVal();
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	std::wstring result = fixEnglishHebrew(copiedData);
	flushFixedString(result);
	pasteResult();
	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	Backupclipboard.Restore();
}

void flushFixedString(const std::wstring& fixedString)
{
	OpenClipboard(NULL);
	EmptyClipboard();
	HGLOBAL memHandle = GlobalAlloc(GMEM_MOVEABLE, fixedString.length() * (sizeof(wchar_t) + 1));
	if (!memHandle) 
	{
		CloseClipboard();
		return;
	}
	memcpy(GlobalLock(memHandle), fixedString.c_str(), fixedString.length() * (sizeof(wchar_t) + 1));
	GlobalUnlock(memHandle);
	SetClipboardData(CF_UNICODETEXT, memHandle);
	CloseClipboard();
	GlobalFree(memHandle);
}

std::wstring fixEnglishHebrew(const std::wstring& data)
{
	std::array<wchar_t, NUM_OF_KEYS> hebrewKeyboard = { L'/', L'\'', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L']', L'[', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L',', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'.'};
	std::array<wchar_t, NUM_OF_KEYS> englishKeyboard = { L'q', L'w', L'e', L'r', L't', L'y', L'u', L'i', L'o', L'p', L'[', L']', L'a', L's', L'd', L'f', L'g', L'h', L'j', L'k', L'l', L';', L'\'', L'z', L'x', L'c', L'v', L'b', L'n', L'm', L',', L'.', L'/' };

	size_t hebrewCounter = 0;
	size_t englishCounter = 0;
	for (auto& chr : data)
	{
		if (std::find(hebrewKeyboard.begin(), hebrewKeyboard.end(), chr) != hebrewKeyboard.end())
		{
			hebrewCounter++;
		}
		if (std::find(englishKeyboard.begin(), englishKeyboard.end(), chr) != englishKeyboard.end())
		{
			englishCounter++;
		}
	}

	return hebrewCounter > englishCounter ? switchKeyboards(data, hebrewKeyboard, englishKeyboard) : switchKeyboards(toLower(data), englishKeyboard, hebrewKeyboard);
}

std::wstring toLower(const std::wstring& data)
{
	std::wstring result(data);
	std::for_each(result.begin(), result.end(), [](wchar_t& c) {
		c = ::tolower(c);
	});

	return result;
}

std::wstring switchKeyboards(const std::wstring& data, const std::array<wchar_t, NUM_OF_KEYS>& source, const std::array<wchar_t, NUM_OF_KEYS>& target)
{
	std::wstring result;
	for (auto& chr : data)
	{
		auto charPos = std::find(source.begin(), source.end(), chr);
		if (charPos != source.end())
		{
			result += target.at(charPos - source.begin());
		}
		else
		{
			result += chr;
		}
	}

	return result;
}

std::wstring getCopiedVal()
{
	OpenClipboard(NULL);
	HANDLE hData = GetClipboardData(CF_UNICODETEXT);
	wchar_t * pszText = static_cast<wchar_t*>(GlobalLock(hData));
	if (pszText != NULL)
	{
		std::wstring text(pszText);

		GlobalUnlock(hData);
		CloseClipboard();

		return text;
	}
	else
	{
		GlobalUnlock(hData);
		CloseClipboard();
		return std::wstring();
	}
}

std::wstring stringToWstring(const char* utf8Bytes)
{
	//setup converter
	using convert_type = std::codecvt_utf8<typename std::wstring::value_type>;
	std::wstring_convert<convert_type, typename std::wstring::value_type> converter;

	//use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
	return converter.from_bytes(utf8Bytes);
}

void copySelectedText()
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Press the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Press the "C" key
	ip.ki.wVk = 'C';
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "C" key
	ip.ki.wVk = 'C';
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}

void selectAll()
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Press the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Press the "V" key
	ip.ki.wVk = 'A';
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "V" key
	ip.ki.wVk = 'A';
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}

void pasteResult()
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Press the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Press the "V" key
	ip.ki.wVk = 'V';
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "V" key
	ip.ki.wVk = 'V';
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}

void waitForCtrlF10(std::condition_variable& f10Cond)
{
	RegisterHotKey(NULL, HOTKEY_ID, MOD_CONTROL, VK_F10);
	while (!globalTeminate)
	{
		std::unique_ptr<tagMSG> msg(new tagMSG);

		GetMessage(msg.get(), NULL, NULL, NULL);
		f10Cond.notify_all();
	}
	UnregisterHotKey(NULL, HOTKEY_ID);
}

void fakeF10Event()
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Press the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Press the "V" key
	ip.ki.wVk = VK_F10;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "V" key
	ip.ki.wVk = VK_F10;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}

BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
	if (CTRL_C_EVENT == fdwCtrlType)
	{
		globalTeminate = true;
		fakeF10Event();
		return TRUE;
	}
	return FALSE;
}