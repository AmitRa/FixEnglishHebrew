#pragma once
#include <iostream>


template <typename T>
class Node
{
public:
	Node<T>(const T& data)
	{
		_data = new T(data);
		_next = nullptr;
	}
	~Node<T>()
	{
		delete _data;
	}

	void setData(const T& data)
	{
		if (data)
		{
			delete _data;
		}
		_data(data);
	}

	T getData() const 
	{
		return *_data;
	}

	void setNext(Node<T>* node)
	{
		_next = node;
	}

	Node<T>* getNext() const
	{
		return _next;
	}

private:
	T* _data;
	Node<T>* _next;
};

template <typename T>
class LinkedList
{
public:
	LinkedList<T>()
	{
		_head = nullptr;
	}

	~LinkedList<T>()
	{
		clear();
	}

	void push_back(const T& data)
	{
		Node<T>* cur = _head;
		if (!cur)
		{
			_head = new Node<T>(data);
			return;
		}
		
		while (cur->getNext() != nullptr)
		{
			cur = cur->getNext();
		}
		cur->setNext(new Node<T>(data));
	}

	Node<T>* getLast() const
	{
		Node<T>* cur = _head;
		if (!cur || !cur->getNext())
			return _head;

		while (cur->_next != nullptr)
		{
			cur = cur->getNext();
		}
		return cur;
	}

	std::unique_ptr<T> pop_back()
	{
		Node<T>* cur = _head;
		if (!cur)
			return std::unique_ptr<T>(nullptr);

		if (!cur->getNext())
		{
			std::unique_ptr<T> reVal(new T(cur->getData()));
			delete cur; // cur is _head
			_head = nullptr;
			return reVal;
		}

		while (cur->getNext()->getNext() != nullptr)
		{
			cur = cur->getNext();
		}
		std::unique_ptr<T> retVal(new T(cur->getNext()->getData()));
		delete cur->getNext();
		cur->setNext(nullptr);
		return retVal;
	}

	void clear()
	{
		while (_head)
		{
			pop_back();
		}
	}

	Node<T>* getHead()
	{
		return _head;
	}

private:
	Node<T>* _head;
};